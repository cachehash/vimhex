# Vimhex

Vimhex is a plugin to improve vim's ability to be used as a hex editor with xxd. Vim ships with xxd, a hex dumping utility that gives it the ability to edit binary files, however Vim is optimized for editing text, not hex dumps. This plugin seeks to address that.

## Syntax Highlighting 

Currently vimhex will autodetect the file extensions .hx or .hex and do some basic syntax highlighting as well as automatically enabling hex-aware increment/decrement and the following below features

## Hex Aware Increment/Decrement

Vimhex overrides the behavior of ctrl+A and ctrl+X for .hx or .hex files to be hex aware and will increment/decrement all numbers as if they are hex and not decimal. 

This feature requires Tim Pope's [vim-repeat](https://github.com/tpope/vim-repeat) plugin to work with the `.` command

## Hex Number Prefix to Commands

Vimhex allows you to enter a hex number instead of a decimal one as the prefix to normal mode commands. Normal if you want to add 15 to the number under the cursor you could simply key in the following:

``` viml
15<C-a>
```

However if you wanted to increment a number with a hex amount such as 0x15 you'd either have to do mental math and convert it to decimal before you enter it or something like this:

``` viml
@=0x15<CR><C-a>
```

As this can be cumbersome if you must do it frequently vimhex shortens this to for hexdump files such as .hx or .hex:

``` viml
0x15<C-a>
```

