function! vimhex#math#inc(re, amt)
	let re = '\c\v%('.a:re.'[0-9a-f])+'
	let ln = line('.')

	let pos = getcurpos()
	"Position the cursor at the end of the match or break
	if !search(re, 'ce', ln)
		call setpos('.', pos)
		"return 'echoerr '.string('No number left on line')
		return
	endif
	normal! m]
	call search(l:re, 'bc', l:ln)
	normal! m[`]
	let start = col("'[")
	let end = col("']")+1

	let line = getline('.')
	let [pre, match, post] = matchlist(line, '\v(.*)(%'.start.'c.*%'.end.'c)(.*)')[1:3]

	let len = len(match)
	"TODO should preserve case. built-in ctrl+a
	"uses the case of the last alpha character
	"e.g. 0xDEADFAc0 increments to 0xdeadfac1
	let replace = printf('%0'.len.'x', '0x'.match + a:amt)[-len:]
	call setline('.', pre.replace.post)
endfunction
function! vimhex#math#doInc(regex, sign)
	function! s:vimhex_inc(type) closure
		return vimhex#math#inc(a:regex, a:sign * v:count1)
	endfunction
	let &operatorfunc = get(funcref('s:vimhex_inc'), 'name')
	return 'g@l'
endfunction

function! vimhex#math#doIncVis(run, amt)
	let repeat = 0
	"let vis = visualmode()
	function! s:vimhex_incvis(type) closure
		let opfunc = &operatorfunc
		if repeat && &virtualedit !=# 'all'
			let virted = &virtualedit
			set virtualedit=all
			"Repeat the operation with virtualedit
			norm! .
			let &virtualedit = virted
			return
		endif
		if a:type ==# 'line'
			"For line based put the open/close marks
			"at the start/end of their lines
			normal! `]$m]`[0m[
		endif
		let poses = split('[]<>', '\m')->map({k,v -> ["'".v, getpos("'".v)]})
		if repeat
			let vis = {'line': 'V', 'char': 'v', 'block': "\<C-v>"}[a:type]
			execute 'normal! '.vis."`]\<Esc>"
		endif
		let num = 0
		for lnr in range(line("'["), line("']"))
			execute lnr.'norm! 0'
			call vimhex#math#inc('%V', (1+num*a:run)*a:amt)
			let num += 1
		endfor
		call map(poses, {k,v-> setpos(v[0], v[1])})
		normal! `[
		let &operatorfunc = opfunc
		let repeat = 1
	endfunction
	let &operatorfunc = get(funcref('s:vimhex_incvis'), 'name')
	return 'g@'
endfunction

function! vimhex#math#readhex()
	let char = nr2char(getchar())
	"Make 0x0 behave like 0
	if char ==# '0'
		normal! 0
		return
	endif
	"Collect entered hex number in buffer
	let buff = ''
	while char =~? '[0-9a-f]'
		let buff .= char
		let char = nr2char(getchar())
	endwhile
	"As soon as non-hex digit is entered convert hex number
	"to decimal and feed the key sequence to vim
	let nr = ('0x' . buff) + 0
	call feedkeys(nr)
	"if the char is not a 'hex delimiter' feed it to vim
	if char !~# g:vimhex_hex_delimiter
		call feedkeys(char)
	endif
	return ''
endfunction
