function! s:call_func(func, ...)
	let keys = ":\<C-u>"
	if mode() =~? "[v\<C-v>]"
		let keys .= "execute 'normal! gv' | "
	endif
	let Func = function(a:func)
	let fname = get(Func, 'name')
	let keys .= 'call '.fname.'('.map(copy(a:000), {k,v->string(v)})->join(', ').")\<CR>"
	return keys
endfunction

function! s:do_search(...)
	return call('s:call_func', ['search'] + a:000)
endfunction

function! vimhex#nav#left(amt)
	return s:do_search('\v%([^: \t][ \t]?){1,'.a:amt.'}%#', 'b', line('.'))
endfunction

function! vimhex#nav#right(amt)
	return s:do_search('\v^%(\S*:.*%#.|\S*%#\S*:)%(.{-,1}[^: \t]){1,'.a:amt.'}', 'e', line('.'))
endfunction
