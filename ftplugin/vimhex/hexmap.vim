nmap <special> <buffer> <C-a> <plug>(VimhexInc)
nmap <special> <buffer> <C-x> <plug>(VimhexDec)

vmap <special> <buffer> <C-a> <plug>(VimhexIncVis)
vmap <special> <buffer> <C-x> <plug>(VimhexDecVis)

vmap <special> <buffer> g<C-a> <plug>(VimhexIncSeq)
vmap <special> <buffer> g<C-x> <plug>(VimhexDecSeq)

map <special> <buffer> 0x <plug>(VimhexReadHex)

map <special> <buffer> <expr> <silent> h vimhex#nav#left(v:count1)
map <special> <buffer> <expr> <silent> l vimhex#nav#right(v:count1)
