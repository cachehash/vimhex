
"Since vim native packages doesn't load an ftplugin directory use
"an autocmd to source ftplugin files explicitly for hex dump
"specific mappings

"TODO currently these files will likely be sourced twice if the
"plugin is sourced with something that uses runtimepath like
"Pathogen. This is harmless as the files in question only set
"up mappings but should be fixed regardless.
let s:dir = expand('<sfile>:p:h:h')
let s:ftdir = s:dir.'/ftplugin/vimhex'

autocmd FileType vimhex call s:BootStrapVimhexPlugin()

function s:BootStrapVimhexPlugin()
	for s:script in split(globpath(s:ftdir, '*.vim'), '\n')
		exec 'source '.fnameescape(s:script)
	endfor
endfunction
