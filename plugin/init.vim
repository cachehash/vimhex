let s:default_hex_delim = '['']'
let g:vimhex_hex_delimiter = exists('g:vimhex_hex_delimiter') ? g:vimhex_hex_delimiter : s:default_hex_delim

