nnoremap <expr> <silent> <Plug>(VimhexInc) vimhex#math#doInc('', 1)
nnoremap <expr> <silent> <Plug>(VimhexDec) vimhex#math#doInc('', -1)

vnoremap <expr> <silent> <Plug>(VimhexIncVis) vimhex#math#doIncVis(0, v:count1)
vnoremap <expr> <silent> <Plug>(VimhexDecVis) vimhex#math#doIncVis(0, -v:count1)
vnoremap <expr> <silent> <Plug>(VimhexIncSeq) vimhex#math#doIncVis(1, v:count1)
vnoremap <expr> <silent> <Plug>(VimhexDecSeq) vimhex#math#doIncVis(1, -v:count1)


nnoremap <expr> <silent> <Plug>(VimhexReadHex) vimhex#math#readhex()
vnoremap <expr> <silent> <Plug>(VimhexReadHex) vimhex#math#readhex()
onoremap <expr> <silent> <Plug>(VimhexReadHex) vimhex#math#readhex()
