if exists("b:current_syntax")
	finish
endif

syntax match hexAddress '^[0-9a-fA-F]*:' contains=hexAddressColon
syntax match hexAddressColon ':' contained

highlight link hexAddress Comment
highlight link hexAddressColon SpecialChar

let b:current_syntax = "hexdump"
